/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */

class PlayerJetfighter extends GameObject
{
    /* Each gameObject MUST have a constructor() and a render() method.        */
    /* If the object animates, then it must also have an updateState() method. */

    constructor(image, centreX)
    {
        super(5); /* as this class extends from GameObject, you must always call super() */

        /* These variables depend on the object */
        this.image = image;
        this.width = 50;
        this.height = 50;
        this.centreX = canvas.width / 2;
        this.centreY = canvas.height / 2;
        this.stepSize = - 1;
        this.rotation = 360;
        this.isRotation = false
        this.isMove = false
    }

    updateState()
    {
        if (this.isRotation){
            this.rotation -= 1;
            if (this.rotation < 1)
            {
                this.rotation = 360;
            }
        } 

        if (this.stepSize != 0 && this.isMove) {
            if (this.centreY < 0 - this.height / 2) {
                this.centreY = canvas.height + this.height / 2
            } else {
                this.centreY += this.stepSize
            }
        }
    }

    render()
    {
        ctx.save();
        ctx.translate(this.centreX, this.centreY);
        ctx.rotate(Math.radians(this.rotation));
        ctx.translate(-this.centreX, -this.centreY);

        ctx.drawImage(this.image, this.centreX - this.width / 2, this.centreY - this.width / 2, this.width, this.height);
        ctx.restore();
    }

    getCentreX()
    {
        return this.centreX;
    }

    getCentreY()
    {
        return this.centreY;
    }

    toggleRotation() {
        this.isRotation = !this.isRotation
    }

    toggleMove() {
        this.isMove = !this.isMove
    }
}