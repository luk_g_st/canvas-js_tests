/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland.             */
/* There should always be a javaScript file with the same name as the html file. */
/* This file always holds the playGame function().                               */
/* It also holds game specific code, which will be different for each game       */





/******************** Declare game specific global data and functions *****************/
/* images must be declared as global, so that they will load before the game starts  */
let backgroundImage = new Image();
backgroundImage.src = "img/map_1.png";

let playerJetImage = new Image();
playerJetImage.src = "img/jetfighter_1.png";

let birdImage = new Image()
birdImage.src = "img/bird.png"

const BACKGROUND = 0;
const WIN_LOSE_MESSAGE = 1;

/* Instead of using gameObject[], we can declare our own gameObject variables */
let bat = null; // we cannot initialise gameObjects yet, as they might require images that have not yet loaded
let player = null;
let worldMap = null;

let fireballs = [];
let numberOfBulletsFired = 0; // no bullets fired yet
/******************* END OF Declare game specific data and functions *****************/







/* Always have a playGame() function                                     */
/* However, the content of this function will be different for each game */
function playGame()
{
    /* We need to initialise the game objects outside of the Game class */
    /* This function does this initialisation.                          */
    /* Specifically, this function will:                                */
    /* 1. initialise the canvas and associated variables                */
    /* 2. create the various game gameObjects,                   */
    /* 3. store the gameObjects in an array                      */
    /* 4. create a new Game to display the gameObjects           */
    /* 5. start the Game                                                */



    /* Create the various gameObjects for this game. */
    /* This is game specific code. It will be different for each game, as each game will have it own gameObjects */

    gameObjects[BACKGROUND] = new StaticImage(backgroundImage, 0, 0, backgroundImage.width, backgroundImage.height);
    worldMap = new WorldMap(backgroundImage)
    gameObjects[gameObjects.length] = worldMap;
    
    player = new PlayerJetfighter(playerJetImage, 100, 0, 100);
    gameObjects[gameObjects.length] = player;

    animatedBird = new AnimatedBird(birdImage, 100, 100, 100, 10)
    gameObjects[gameObjects.length] = animatedBird
    /* END OF game specific code. */


    /* Always create a game that uses the gameObject array */
    let game = new JetfighterCanvasGame();

    /* Always play the game */
    game.start();

    
    /* If they are needed, then include any game-specific mouse and keyboard listners */
    document.addEventListener("keydown", function (e)
    {
        var stepSize = 0.5;

        if (e.keyCode === 37)  // left
        {
            worldMap.setSpeedX(worldMap.getSpeedX() + stepSize);
        }
        else if (e.keyCode === 39) // right
        {
            worldMap.setSpeedX(worldMap.getSpeedX() - stepSize);
        }
        else if (e.keyCode === 38)  // left
        {
            worldMap.setSpeedY(worldMap.getSpeedY() + stepSize);
        }
        else if (e.keyCode === 40) // right
        {
            worldMap.setSpeedY(worldMap.getSpeedY() - stepSize);
        } 
        else if (e.keyCode === 32) // space bar
        {
            player.toggleRotation()
            // player.toggleMove()
        }
    });


    /* add event listners for input changes */
    document.getElementById("Red").addEventListener("change", function ()
    {
        gameObjects[0].setRedLevel(document.getElementById("Red").value);
    });
    
    document.getElementById("Green").addEventListener("change", function ()
    {
        gameObjects[0].setGreenLevel(document.getElementById("Green").value);
    });
    
    document.getElementById("Blue").addEventListener("change", function ()
    {
        gameObjects[0].setBlueLevel(document.getElementById("Blue").value);
    });

    document.getElementById("MoveToggle").addEventListener("click", function ()
    {
        player.toggleMove()
    });
}