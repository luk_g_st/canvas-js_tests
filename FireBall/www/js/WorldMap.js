/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */

class WorldMap extends GameObject
{
    /* Each gameObject MUST have a constructor() and a render() method.        */
    /* If the object animates, then it must also have an updateState() method. */

    constructor(image, centreX)
    {
        super(5); /* as this class extends from GameObject, you must always call super() */

        /* These variables depend on the object */
        this.image = image;
        this.width = image.width;
        this.height = image.height;
        this.X = 0;
        this.Y = 0;
        this.speedX = 0;
        this.speedY = 0;
    }

    updateState()
    {
        this.X += this.speedX
        this.Y += this.speedY
    }

    render()
    {
        ctx.drawImage(this.image, this.X, this.Y, this.width, this.height);
        ctx.restore();
    }

    setX(X)
    {
        this.X = X;
    }

    setY(Y)
    {
        this.Y = Y;
    }

    getX()
    {
        return this.X;
    }

    getY()
    {
        return this.Y;
    }

    setSpeedX(speedX)
    {
        this.speedX = speedX;
    }

    setSpeedY(speedY)
    {
        this.speedY = speedY;
    }

    getSpeedX()
    {
        return this.speedX;
    }

    getSpeedY()
    {
        return this.speedY;
    }
}