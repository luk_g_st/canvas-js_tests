/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */

class StaticImage extends GameObject
{
    /* Each gameObject MUST have a constructor() and a render() method.        */
    /* If the object animates, then it must also have an updateState() method. */

    constructor(image, x, y, width, height)
    {
        super(null); /* as this class extends from GameObject, you must always call super() */

        /* These variables depend on the object */
        this.image = image;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.Red = 0;
        this.Blue = 0;
        this.Green = 0;
    }

    render()
    {
        ctx.drawImage(this.image, this.x, this.y, this.width, this.height);

        let imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        for (let i = 0; i < imageData.data.length; i += 4) {
            imageData.data[i + 0] = imageData.data[i + 0] + this.Red;
            imageData.data[i + 1] = imageData.data[i + 1] + this.Green;
            imageData.data[i + 2] = imageData.data[i + 2] + this.Blue;
            imageData.data[i + 3] = 255;
        }

        ctx.putImageData(imageData, 0, 0);
    }

    setRedLevel(newRed)
    {
        this.Red = parseInt(newRed);
    }
    
    setGreenLevel(newGreen)
    {
        this.Green = parseInt(newGreen);
    }
    
    setBlueLevel(newBlue)
    {
        this.Blue = parseInt(newBlue);
    }
}